// ------------------------------
// 'C' Daniel Bauer
// Includes FastLEDLib -> "https://github.com/FastLED/FastLED"
// Arduino - Ufo
// 28.09.2014
// ------------------------------
// schnell drehend immer andere farbe
// landelicht -> auch bei leer akku nur mit blinken
// 

// Lib - Defines
#include "FastLED.h"

// Defines
// Debug mode
#define DEBUG

// How many leds in your strip?
#define NUM_LEDS 60

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 10
//#define CLOCK_PIN 9

// Defines the pin, which controls the current effect
#define RC_PROGRAM_PIN 2
#define RC_PROGRAM_PIN_INTERRUPT 1
#define BUTTON_PROGRAM_PIN 3
#define BUTTON_PROGRAM_PIN_INTERRUPT 0

// Defines the pin of the micro onboard led
#define ONBOARD_LED_PIN 13

// Defines the low voltage input pins
#define LIPO_VOLTAGE_PIN A6 
float LIPO_VOLTAGE_MULTIPLIER = 20.2;
#define LED_VOLTAGE_PIN A0
static float LED_VOLTAGE_MULTIPLIER = 5.0;
#define RC_VOLTAGE_PIN A1
static float RC_VOLTAGE_MULTIPLIER = 5.0;

// Define the array of leds
CRGB leds[NUM_LEDS];

// Include the current state of the program
int state=1;
int pushed = LOW;
int flange = LOW;

// Init
void setup() { 
  
       // Init FastLED
       FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);

       // Init PinMode
       pinMode(ONBOARD_LED_PIN, OUTPUT);
       pinMode(RC_PROGRAM_PIN, INPUT);
       attachInterrupt(RC_PROGRAM_PIN_INTERRUPT, stateInterrupt, CHANGE);
       pinMode(BUTTON_PROGRAM_PIN, INPUT);
       attachInterrupt(BUTTON_PROGRAM_PIN_INTERRUPT, stateInterrupt1, CHANGE);
       
       // Set initial LED state
       digitalWrite(ONBOARD_LED_PIN, LOW);
       
#ifdef DEBUG
       // Initialize serial communication at 9600 bits per second:
       Serial.begin(9600);
       Serial.println("Hello from Arduino");
       
       // Printout Voltage
       Serial.print("LED Voltage: ");
       Serial.print(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER));
       Serial.print("\n");  
  
       // Printout Voltage
       Serial.print("Lipo Voltage: ");
       Serial.print(getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER));
       Serial.print("\n");
   
       // Printout Voltage
       Serial.print("RC Voltage: ");
       Serial.print(getVoltage(RC_VOLTAGE_PIN, RC_VOLTAGE_MULTIPLIER));
       Serial.print("\n");     
#endif
}

// Cycle
void loop() { 
  
      // Check state changed
       if(flange == HIGH && pushed == LOW)
       {
           state++;
           if(state >= 6) state = 1;
#ifdef DEBUG
           // Current state
           Serial.print("State Signal: ");
           Serial.print(state);
           Serial.print("\n");   
#endif
        flange = LOW;
       }    
        
        // Check LED and Lipo voltage
        if(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER) <= 4.8 || getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER) <= 11.0)
        {
          // LED off
          digitalWrite(ONBOARD_LED_PIN, LOW); 
          
          // Low voltage
          state = 0;
        }
        else
        {
          // LED on
          digitalWrite(ONBOARD_LED_PIN, HIGH);

          // Voltage OK
          if(state == 0) state++;          
        }
        
#ifdef DEBUG
       Serial.println("-------------");
       // Printout Voltage
       Serial.print("LED Voltage: ");
       Serial.print(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER));
       Serial.print("\n"); 
 
       // Printout Voltage
       Serial.print("Lipo Voltage: ");
       Serial.print(getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER));
       Serial.print("\n");    
     
       // Printout Voltage
       Serial.print("RC Voltage: ");
       Serial.print(getVoltage(RC_VOLTAGE_PIN, RC_VOLTAGE_MULTIPLIER));
       Serial.print("\n");     
       
       // Current state
       Serial.print("Program State: ");
       Serial.print(state);
       Serial.print("\n");
#endif        
        // -----------------------------
        // Do current state
        
        long r_red = random8();
        long r_green = random8();
        long r_blue = random8();
        long r_brightness = random8();
        
        switch(state)
        {
  
          case 0: // Low voltage, flash landing light
          allLEDoff();
          
              // turn on led, then pause
              leds[0] = CRGB::Green;
              leds[1] = CRGB::Green;
              leds[NUM_LEDS-1] = CRGB::Red;
              leds[NUM_LEDS-2] = CRGB::Red;
              
              leds[(NUM_LEDS/2)-1] = CRGB::White;
              leds[(NUM_LEDS/2)] = CRGB::White;
              leds[(NUM_LEDS/2)+1] = CRGB::White;
              FastLED.show();
              delay(50);
              
              // Now turn the LED off, then pause
              leds[0] = CRGB::Black;
              leds[1] = CRGB::Black;
              leds[NUM_LEDS-1] = CRGB::Black;
              leds[NUM_LEDS-2] = CRGB::Black;
  
              leds[(NUM_LEDS/2)-1] = CRGB::Black;
              leds[(NUM_LEDS/2)] = CRGB::Black;
              leds[(NUM_LEDS/2)+1] = CRGB::Black;
              FastLED.show();
              delay(50);
          break;
          
          case 1: // Slide LED fast
          allLEDoff();
        	  
            // slide the led in one direction
            for(int i = 0; i < NUM_LEDS; i++) {
              // Set the i'th led to red 
              
              leds[i].r = r_red; // red
              leds[i].g = r_green; // green
              leds[i].b = r_blue; // blue
              //leds[i] = CRGB::HotPink;
              // Show the leds
              FastLED.show();
              // now that we've shown the leds, reset the i'th led to black
              leds[i] = CRGB::Black;
              // Wait a little bit before we loop around and do it again
              delay(5);
            }
          break;
          
          case 2: // Landing light
          //allLEDoff();
          
            // turn on led, then pause
            leds[0] = CRGB::Green;
            leds[1] = CRGB::Green;
            leds[NUM_LEDS-1] = CRGB::Red;
            leds[NUM_LEDS-2] = CRGB::Red;
            
            leds[(NUM_LEDS/2)-1] = CRGB::White;
            leds[(NUM_LEDS/2)] = CRGB::White;
            leds[(NUM_LEDS/2)+1] = CRGB::White;
            FastLED.show();
            delay(100);
              
            // Now turn the LED off, then pause
            leds[0] = CRGB::Black;
            leds[1] = CRGB::Black;
            leds[NUM_LEDS-1] = CRGB::Black;
            leds[NUM_LEDS-2] = CRGB::Black;

            leds[(NUM_LEDS/2)-1] = CRGB::Black;
            leds[(NUM_LEDS/2)] = CRGB::Black;
            leds[(NUM_LEDS/2)+1] = CRGB::Black;
            delay(50);
          break;
          
          case 3: // Round loop
          allLEDoff();
          for(int i=0;i<=5;i++)
          {
            leds[random(0,NUM_LEDS)] = CHSV( r_blue, r_blue, r_brightness);
            //leds[i] = CRGB::Red;
          }
            FastLED.show();
            delay(20);
          break;
          
          case 4: // Flash every fourth led
          allLEDoff();
          
            delay(100);
            // put every fourth led on
            for(int i = 0; i < NUM_LEDS; i += 4) {
              // Set the i'th led to red 
              leds[i].r = r_red; // red
              leds[i].g = r_green; // green
              leds[i].b = r_blue; // blue
            }
            // Show the leds
            FastLED.show();
            delay(100);             
            
          break;
          
          case 5: // Round loop slow
          allLEDoff();
          
            // slide the led in one direction
            for(int i = 6; i < NUM_LEDS; i+=random(1,4)) {
              // Set the i'th led to red 
              leds[i] = CHSV( r_blue, r_blue, 255); 
              leds[i-1] = CHSV( r_blue, r_blue, 200); 
              leds[i-2] = CHSV( r_blue, r_blue, 150); 
              leds[i-3] = CHSV( r_blue, r_blue, 100);
              leds[i-4] = CHSV( r_blue, r_blue, 80);
              leds[i-5] = CHSV( r_blue, r_blue, 50);
              leds[i-6] = CHSV( r_blue, r_blue, 20);
              
              // Set brightness
            //FastLED.setBrightness(random8());
            // Show the leds
            FastLED.show();
            //leds[i] = CRGB::Black;
            delay(20);  
            allLEDoff();
          }
                
          break;
        }
}

// state interrupt
void stateInterrupt1()
{   
   pushed = digitalRead(BUTTON_PROGRAM_PIN);
   
   if(flange==LOW)
   {
     flange = HIGH;
     Serial.println("State Signal: BUTTON"); 
   }
}

// 1524 Normal
// 1920 Positiv
// 1116 Negatic
// PWM
unsigned long time = 0;
unsigned long time_start = 0;
unsigned int noise_h = 0;
unsigned int noise_l = 0;
// state interrupt
void stateInterrupt()
{   
    // Read RC PWM 
    if(digitalRead(RC_PROGRAM_PIN) == HIGH)
    {
        time_start = micros();
    }
    if(digitalRead(RC_PROGRAM_PIN) == LOW)
    {
      time = micros() - time_start;
      //Serial.println(time);
        
        if(time >= 1800 && time <= 2100)
        {
          noise_h++;
          if(noise_h >= 4)
          {
            pushed = HIGH;
            Serial.println("State Signal: HIGH");
            noise_h = 0;
          }
        }
        else if(time <= 1300 && time >= 1000) 
        {
          noise_l++;
          if(noise_l >= 4)
          {
            pushed = LOW;
            Serial.println("State Signal: LOW");
            noise_l = 0;
          }
        }
        else
        {
          noise_l = 0;
          noise_h = 0;
          //Serial.println("State Signal: Middle ");
        }
    }
    
    // Check fpr state change
    // Only use flange
    if(pushed == HIGH && flange == LOW)
    {
        flange = HIGH;
    }

}

// Load voltage
float getVoltage(char analogPin, float multiplier)
{
    // Init analogRead 
    int analogValue = analogRead(analogPin);
    
    // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
    float currentVoltage = analogValue * (multiplier / 1023.0);
	
    return currentVoltage;
}

// All black
void allLEDoff()
{
  // First slide the led in one direction
  for(int i = 0; i < NUM_LEDS; i++) { 
    leds[i] = CRGB::Black;
  }
  // Show the leds
  FastLED.show();
}

