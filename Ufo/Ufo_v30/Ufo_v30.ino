// ------------------------------
// 'C' Daniel Bauer
// Includes FastLEDLib -> "https://github.com/FastLED/FastLED"
// Arduino - Ufo
// 28.09.2014
// ------------------------------
// 
// ------------------------------
// FastLED Lib
#include "FastLED.h"

// Defines debug output
#define DEBUG

// How many leds in your strip?
#define NUM_LEDS 120

// Defines the array of leds
CRGB leds[NUM_LEDS];

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 10
//#define CLOCK_PIN 9

// Pindefs - Signal input
#define RC_PROGRAM_PIN 2
#define RC_PROGRAM_PIN_INTERRUPT 1
#define BUTTON_PROGRAM_PIN 3
#define BUTTON_PROGRAM_PIN_INTERRUPT 0

// Pindefs - Onboard LED
#define ONBOARD_LED_PIN 13

// Pindefs - Voltage measuring 
static float LIPO_VOLTAGE_MULTIPLIER = 20.2;
#define LED_VOLTAGE_PIN A5
static float LED_VOLTAGE_MULTIPLIER = 5.0;
#define RC_VOLTAGE_PIN A1
static float RC_VOLTAGE_MULTIPLIER = 5.0;
#define LIPO_VOLTAGE_PIN A6 

// Defines minimum required voltage
#define MIN_LIPO_VOLTAGE 7.0 //7.0
#define MIN_RC_VOLTAGE 4.8
#define MIN_LED_VOLTAGE 4.8

// ------------------------------
// Variables
int state = 1;
boolean pushed = LOW;
boolean flange = LOW;
int flash_var = 0;
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
boolean LastButtonState = LOW;
boolean LastRCState = LOW;
unsigned long lastExec = 0;
boolean flipFlop = HIGH; 

// ------------------------------
// Init
void setup() { 
  
       // Init FastLED
       // If the displayed LED color is wrong, change the direction of the LEDs to (GRB, RGB, BRG, ....)
       FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);

       // Init PinMode
       pinMode(ONBOARD_LED_PIN, OUTPUT);
       pinMode(RC_PROGRAM_PIN, INPUT);
       attachInterrupt(RC_PROGRAM_PIN_INTERRUPT, stateInterrupt, CHANGE);
       pinMode(BUTTON_PROGRAM_PIN, INPUT);
       
       // Set initial onboard LED state
       digitalWrite(ONBOARD_LED_PIN, LOW);
       
       // Turn all LEDs off
       allLEDoff();
       
#ifdef DEBUG
       // Initialize serial communication at 9600 bits per second:
       Serial.begin(9600);
       Serial.println("Hello PC from Arduino");
       
       // Printout LED voltage
       Serial.println("-------------");
       Serial.print("LED Voltage: ");
       Serial.print(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER));
       Serial.print("\n");  
  
       // Printout Lipo voltage
       Serial.print("Lipo Voltage: ");
       Serial.print(getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER));
       Serial.print("\n");
   
       // Printout RC voltage
       Serial.print("RC Voltage: ");
       Serial.print(getVoltage(RC_VOLTAGE_PIN, RC_VOLTAGE_MULTIPLIER));
       Serial.print("\n");     
#endif
}

// ------------------------------
// Cycle
void loop() { 

        //delay(20);
        // Check if state change
        checkButton();
  
        // Check LED and Lipo voltage
        if(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER) <= MIN_LED_VOLTAGE || getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER) <= MIN_LIPO_VOLTAGE) {
          // LED off
          digitalWrite(ONBOARD_LED_PIN, LOW); 
          
          // Low voltage
          state = 0;
        }
        else {
          // LED on
          digitalWrite(ONBOARD_LED_PIN, HIGH);

          // Voltage OK
          if(state == 0) state = 1;          
        }
        
#ifdef DEBUG
       // Printout LED voltage
       Serial.println("-------------");
       Serial.print("LED Voltage: ");
       Serial.print(getVoltage(LED_VOLTAGE_PIN, LED_VOLTAGE_MULTIPLIER));
       Serial.print("\n"); 
 
       // Printout Lipo voltage
       Serial.print("Lipo Voltage: ");
       Serial.print(getVoltage(LIPO_VOLTAGE_PIN, LIPO_VOLTAGE_MULTIPLIER));
       Serial.print("\n");    
     
       // Printout RC voltage
       Serial.print("RC Voltage: ");
       Serial.print(getVoltage(RC_VOLTAGE_PIN, RC_VOLTAGE_MULTIPLIER));
       Serial.print("\n");     
       
       // Current state
       Serial.print("Program State: ");
       Serial.print(state);
       Serial.print("\n");
#endif        
        // Random color for each duration
        static long r_red = random8();
        static long r_green = random8();
        static long r_blue = random8();
        static long r_brightness = random8();
        
        // Switch to the current states
        // To add more effects add a new case
        switch(state) {
  
          case 0: // Low voltage, flash landing light
          
          // Check time
          if(millis() - lastExec >= 100) {

            if(flipFlop) {
              allLEDoff();
              flipFlop = LOW;
              lastExec = millis();
            } 
            else {
              
               // Turn on white LEDs on the front
              leds[0] = CRGB::White;
              leds[1] = CRGB::White;
              leds[NUM_LEDS-1] = CRGB::White;
              leds[NUM_LEDS-2] = CRGB::White;
              
              // Turn on red and yellow LEDs on the side
              leds[(NUM_LEDS/4)-1] = CRGB::Red;
              leds[(NUM_LEDS/4)] = CRGB::Red;
              leds[(NUM_LEDS-(NUM_LEDS/4))-1] = CRGB::Blue;
              leds[(NUM_LEDS-(NUM_LEDS/4))] = CRGB::Blue;
              
              // Turn on green LEDS on the back
              leds[(NUM_LEDS/2)-2] = CRGB::Green;
              leds[(NUM_LEDS/2)-1] = CRGB::Green;
              leds[(NUM_LEDS/2)+1] = CRGB::Green;
              leds[(NUM_LEDS/2)+2] = CRGB::Green;
              
              // Show the leds
              FastLED.show();

              flipFlop = HIGH;
              lastExec = millis();
            }
          }
          break;
          
          case 1: // Slide four LEDs fast

            for(int i = 0; i < (NUM_LEDS/4); i++) {
              
              // Set the i'th led
              leds[i].r = r_red;    // random red
              leds[i].g = r_green;  // random green
              leds[i].b = r_blue;   // random blue
              
              // Set the i'th led
              leds[(NUM_LEDS/4)+i].r = r_red;    // random red
              leds[(NUM_LEDS/4)+i].g = r_green;  // random green
              leds[(NUM_LEDS/4)+i].b = r_blue;   // random blue
              
              // Set the i'th led
              leds[(NUM_LEDS/2)+i].r = r_red;    // random red
              leds[(NUM_LEDS/2)+i].g = r_green;  // random green
              leds[(NUM_LEDS/2)+i].b = r_blue;   // random blue
              
              // Set the i'th led
              leds[(NUM_LEDS-(NUM_LEDS/4))+i].r = r_red;    // random red
              leds[(NUM_LEDS-(NUM_LEDS/4))+i].g = r_green;  // random green
              leds[(NUM_LEDS-(NUM_LEDS/4))+i].b = r_blue;   // random blue
              
              // Show the leds
              FastLED.show();
              
              // now that we've shown the leds, reset the i'th led to black
              leds[i] = CRGB::Black;
              leds[(NUM_LEDS/4)+i] = CRGB::Black;
              leds[(NUM_LEDS/2)+i] = CRGB::Black;
              leds[(NUM_LEDS-(NUM_LEDS/4))+i] = CRGB::Black;
              
              // Wait a little bit before we loop around and do it again
              delay(5);
            }
          break;
          
          case 2: // Landing light - right green, left red

            // Check time
            if(millis() - lastExec >= 100) {
              lastExec = millis(); 
              
              // Turn on white LEDs on the front
              leds[0] = CRGB::White;
              leds[1] = CRGB::White;
              leds[NUM_LEDS-1] = CRGB::White;
              leds[NUM_LEDS-2] = CRGB::White;
              
              // Turn on red and yellow LEDs on the side
              leds[(NUM_LEDS/4)-1] = CRGB::Red;
              leds[(NUM_LEDS/4)] = CRGB::Red;
              leds[(NUM_LEDS-(NUM_LEDS/4))-1] = CRGB::Blue;
              leds[(NUM_LEDS-(NUM_LEDS/4))] = CRGB::Blue;
              
              // Turn on green LEDS on the back
              leds[(NUM_LEDS/2)-2] = CRGB::Green;
              leds[(NUM_LEDS/2)-1] = CRGB::Green;
              leds[(NUM_LEDS/2)+1] = CRGB::Green;
              leds[(NUM_LEDS/2)+2] = CRGB::Green;
              
              // Show the leds
              FastLED.show();
            }
          break;
          
          case 3: // Wild flasher

            // Check time
            if(millis() - lastExec >= 70) {
              lastExec = millis();
             
              // Turn all LEDs off
              allLEDoff();
            
              // Set the flashing LEDs
              for(int i = 0;i <= 6;i++)
              {
                leds[random(0,NUM_LEDS)] = CHSV( r_blue, r_blue, r_brightness);
              }
            
              // Show the leds
              FastLED.show();
            }
          break;
          
          case 4: // Flash every x led
          
          // Turn all LEDs off
          allLEDoff();
          
            // Wait a little bit
            delay(20);
            
            // Turn every x led on
           for(int i = flash_var; i < NUM_LEDS; i += 10) {
              // Set the i'th led 
              leds[i].r = r_red;     // random red
              leds[i].g = r_green;   // random green
              leds[i].b = r_blue;    // random blue
            }
            
            // Show the leds
            FastLED.show();
            flash_var++;
            if(flash_var >= 10) {
              flash_var = 0;
            }
            
            // Wait a little bit
            delay(20);             
          break;
        }
}

// ------------------------------
// Button interrupt
void checkButton() {
     
  // Check if state changed
  boolean reading = digitalRead(BUTTON_PROGRAM_PIN);

  // Check if state changed
  if (flange != reading) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  // Reset LastButtonState
  if (LastButtonState == HIGH && reading == LOW) {
    LastButtonState = LOW;
  }

  // Check if state is for longet there
  if ((millis() - lastDebounceTime) >= debounceDelay) {

      // If state is pressed
      if(reading == HIGH && LastButtonState == LOW) {

          // Increase state
          state++;
          if(state >= 5) {
            state = 1;
          }
          LastButtonState = HIGH;
#ifdef DEBUG
          // Current state
          Serial.println("-------------");
          Serial.print("Button State Signal: ");
          Serial.print(state);
          Serial.print("\n");   
#endif
      }    
  }
  flange = reading;
}

// ------------------------------
// PWM interrupt
unsigned long ttime = 0;
unsigned long time_start = 0;
unsigned int noise_h = 0;
unsigned int noise_l = 0;

void stateInterrupt() {   
  
  // Check if RC receiver is plugged in
  if(getVoltage(RC_VOLTAGE_PIN, RC_VOLTAGE_MULTIPLIER) >= MIN_RC_VOLTAGE)
  {
    // Read RC PWM 
    if(digitalRead(RC_PROGRAM_PIN) == HIGH)
    {
        time_start = micros();
    }
    
    // Read RC PWM
    if(digitalRead(RC_PROGRAM_PIN) == LOW)
    {
        ttime = micros() - time_start;
        //Serial.println(time);
             
        if(ttime >= 1800 && ttime <= 2100)
        {
          noise_h++;
          if(noise_h >= 2)
          {
            pushed = HIGH;
            Serial.println("State Signal: HIGH");
            noise_h = 0;
          }
        }
        else if(ttime <= 1300 && ttime >= 1000) 
        {
          noise_l++;
          if(noise_l >= 2)
          {
            pushed = LOW;
            Serial.println("State Signal: LOW");
            noise_l = 0;
          }
        }
        else
        {
          noise_l = 0;
          noise_h = 0;
        }
    }
  }
    
    // Check if state changed
    // Only use flange
    if(pushed == HIGH && LastRCState == LOW) {
        // Increase state
        state++;
        if(state >= 5) {
          state = 1;
        }
        LastRCState = HIGH;
#ifdef DEBUG
        // Current state
        Serial.println("-------------");
        Serial.print("RC State Signal: ");
        Serial.print(state);
        Serial.print("\n");   
#endif
      }    

    //Reset state
    if(LastRCState == HIGH && pushed == LOW) {
      LastRCState = LOW;
    }
}

// ------------------------------
// GetVoltage
float getVoltage(char analogPin, float multiplier) {
    // Init analogRead 
    int analogValue = analogRead(analogPin);
    
    // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
    float currentVoltage = analogValue * (multiplier / 1023.0);

    // Return the value	
    return currentVoltage;
}

// ------------------------------
// Turn all LEDs off
void allLEDoff() {
  // Set all LEDs to black
  for(int i = 0; i < NUM_LEDS; i++)
  { 
    leds[i] = CRGB::Black;
  }
  
  // Show the leds
  FastLED.show();
}

