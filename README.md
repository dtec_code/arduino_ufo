## README - ARDUINO UFO ##

![20141026_125027.jpg](https://bitbucket.org/repo/orMxK5/images/1003965974-20141026_125027.jpg)
![20141026_124454.jpg](https://bitbucket.org/repo/orMxK5/images/3402850665-20141026_124454.jpg)


Look at the [YouTube Video 1](https://www.youtube.com/watch?v=mdAKn5Nahos)
Look at the [YouTube Video 2](https://www.youtube.com/watch?v=y53nOmrqe-I)


#### ARDUINI MICRO BASED RC PLANE LED CONTROLLER #####

Arduino Micro basierter Kontroller für RGB LED (Unterstütze Typen stehen weiter unten) Streifen. Die Effekte können über einen Kanal eines RC Empfängers über PWM oder über den Taster auf der rechten Seite gewechselt werden. Weitere Effekte können im Arduino Projekt eingebaut werden. Unterspannung der angeschlossenen Spannungsversorgung wird durch blinkende Lichter angezeigt.

Arduino Micro based controller for RGB LED (Chipsets see below) stripes. The current effect can be changed by an RC reciever channel over PWM or by the button on the right of the shield. More effects can be added at the Arduino project. Under voltage of the Lipos the RC and the LEDs are shown as flashing lights.

For led chips like Neopixels, which have a data line, ground, and power, you just
need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
ground, and power), like the LPD8806, define both DATA_PIN and CLOCK_PIN
CLOCK_PIN at 9
DATA_PIN at 10

Für Chips wie Neopixel, welche nur eine Datenleitung haben, muss nur der DATA_PIN definiert werden.
Andere SPI basierte Chips, mit Daten- und Clockleitung benötigen dazu noch den CLOCK_PIN.

For the first use of WS2812B LED stripes, connect the stripes to +5V, GND and Data. Observe the connection direction
Connect the source voltage with +12V and GND. Minimum required source voltage is 6,5V and can be up to 25V
Note the warming!!!

Für die erste Verwendung von WS2812B LEDs müssen die LEDs mit +5V, GND und Data verbunden werden. Dabei muss auf die Polung geachtet werden.
Die Versorgungsspannung muss mit +12V und GND verbunden werden. Minimale Versorgungsspannung ist 6,5V und kann bis zu 25V betragen.

I tried to implement it as easy as possible
Please use Ufo_v30.ino - Tag 2.0

Ich habe versucht, es so einfach wie möglich zu programmieren
Bitte die Ufo_v30.ino - Tag 2.0 verwenden

#### Based on: ####
* FastLED [ Link ](https://github.com/FastLED/FastLED)
- Arduino IDE 1.0.6 [ Link ](http://arduino.cc/en/Main/Software)
- Arduino Micro [ Link ](http://arduino.cc/en/Main/ArduinoBoardMicro)
+ Ufo Shield
* RC receiver

#### Downloads ####
* [Link](https://bitbucket.org/dtec_code/arduino_ufo/downloads)

#### Available Chipsets ####
* DMX,
+ TM1809,
+ TM1804,
+ TM1803,
+ WS2811,
+ WS2812,
+ WS2812B,
+ WS2811_400,
+ NEOPIXEL,
+ UCS1903

* LPD8806,
+ WS2801,
+ SM16716

#### Info ####
+ Free to use...
- If you have questions -> contact me